package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Physician;
import au.com.paycorp.service.echannelling.dao.PhysicianDao;
import static au.com.paycorp.service.echannelling.service.helper.Expression.idIsOfPhysician;

import au.com.paycorp.service.interfac.base.annotation.Processor;
import au.com.paycorp.service.interfac.base.annotation.ActionProcessor;
import au.com.paycorp.service.interfac.base.ExpressionManager;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServiceResponse;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service
@Processor (resource = "PHYSICIAN_SERVICE")
public class PhysicianService extends AbstractService<Integer, Physician, PhysicianDao> {
    
    @Autowired
    private PhysicianDao physicianDao;
    
    public PhysicianService() {
    }
    
    @Transactional
    @ActionProcessor(action = "LIST")
    public ServiceResponse list(ServiceRequest serviceRequest) {
        return super.list(serviceRequest, Physician.class, physicianDao);
    }
    
    @Transactional
    @ActionProcessor(action = "FETCH")
    public ServiceResponse fetch(ServiceRequest serviceRequest) {
        return super.fetch(serviceRequest, Physician.class, physicianDao);
    }
    
    @Transactional
    @ActionProcessor(action = "ADD")
    public ServiceResponse add(ServiceRequest serviceRequest) {
        return super.add(serviceRequest, Physician.class, physicianDao);
    }
    
    @Transactional
    @ActionProcessor(action = "CHANGE")
    public ServiceResponse change(ServiceRequest serviceRequest) {
        return super.change(serviceRequest, Physician.class, physicianDao);
    }
    
    @Transactional
    @ActionProcessor(action = "DELETE")
    public ServiceResponse delete(ServiceRequest serviceRequest) {
        return super.delete(serviceRequest, Physician.class, physicianDao);
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to add passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasAddPermission(ServiceRequest serviceRequest, Physician physician) {
        if (serviceRequest == null || physician == null) {
            return false;
        }
        return true;
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to fetch, change, and delete passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasPermission(ServiceRequest serviceRequest, Physician physician) {
        if (serviceRequest == null || physician == null) {
            return false;
        }
        ExpressionManager<Physician> expressionManager = new ExpressionManager<>(Physician.class);
        applyEntityExists(expressionManager, physician.getId());
        return physicianDao.count(expressionManager.buildExpression()) > 0;
    }
    
    /*--------------------------------------------------------------------------
     inspects whether the entity exists with passed 'physician' pk
     --------------------------------------------------------------------------*/
    private void applyEntityExists(ExpressionManager<Physician> expressionManager, Integer pk) {
        if (expressionManager == null || pk == null) {
            return;
        }
        /*----------------------------------------------------------------------
         develops a new boolean-expression to validate 'id' as the
         primary-key
         ---------------------------------------------------------------------*/
        expressionManager.addExpression(idIsOfPhysician(pk));
    }

    /*--------------------------------------------------------------------------
    applies further filtering for non-staff users
    --------------------------------------------------------------------------*/
    @Override
    protected void applyNonStaffFilter(ExpressionManager<Physician> expressionManager, ServiceRequest serviceRequest) {
        // no further filtering applied
    }
    
}