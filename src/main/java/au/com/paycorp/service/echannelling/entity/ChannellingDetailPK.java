package au.com.paycorp.service.echannelling.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.google.gson.GsonBuilder;

@Embeddable
public class ChannellingDetailPK implements Serializable {
    
    @Basic(optional = false)
    @Column(name = "hospital_id")
    private int hospitalId;
    
    @Basic(optional = false)
    @Column(name = "physician_id")
    private int physicianId;
    
    @Basic(optional = false)
    @Column(name = "startat")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startat;
    
    @Basic(optional = false)
    @Column(name = "patient_id")
    private int patientId;

    public ChannellingDetailPK() {
    }

    public ChannellingDetailPK(int hospitalId, int physicianId, Date startat, int patientId) {
        this.hospitalId = hospitalId;
        this.physicianId = physicianId;
        this.startat = startat;
        this.patientId = patientId;
    }

    public int getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(int hospitalId) {
        this.hospitalId = hospitalId;
    }

    public int getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(int physicianId) {
        this.physicianId = physicianId;
    }

    public Date getStartat() {
        return startat;
    }

    public void setStartat(Date startat) {
        this.startat = startat;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) hospitalId;
        hash += (int) physicianId;
        hash += (startat != null ? startat.hashCode() : 0);
        hash += (int) patientId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ChannellingDetailPK)) {
            return false;
        }
        ChannellingDetailPK other = (ChannellingDetailPK) object;
        if (this.hospitalId != other.hospitalId) {
            return false;
        }
        if (this.physicianId != other.physicianId) {
            return false;
        }
        if ((this.startat == null && other.startat != null) || (this.startat != null && !this.startat.equals(other.startat))) {
            return false;
        }
        if (this.patientId != other.patientId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }
    
}
