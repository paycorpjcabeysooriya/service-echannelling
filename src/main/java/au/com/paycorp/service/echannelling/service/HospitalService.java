package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Hospital;
import au.com.paycorp.service.echannelling.dao.HospitalDao;
import static au.com.paycorp.service.echannelling.service.helper.Expression.idIsOfHospital;

import au.com.paycorp.service.interfac.base.annotation.Processor;
import au.com.paycorp.service.interfac.base.annotation.ActionProcessor;
import au.com.paycorp.service.interfac.base.ExpressionManager;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServiceResponse;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service
@Processor (resource = "HOSPITAL_SERVICE")
public class HospitalService extends AbstractService<Integer, Hospital, HospitalDao> {
    
    @Autowired
    private HospitalDao hospitalDao;
    
    public HospitalService() {
    }
    
    @Transactional
    @ActionProcessor(action = "LIST")
    public ServiceResponse list(ServiceRequest serviceRequest) {
        return super.list(serviceRequest, Hospital.class, hospitalDao);
    }
    
    @Transactional
    @ActionProcessor(action = "FETCH")
    public ServiceResponse fetch(ServiceRequest serviceRequest) {
        return super.fetch(serviceRequest, Hospital.class, hospitalDao);
    }
    
    @Transactional
    @ActionProcessor(action = "ADD")
    public ServiceResponse add(ServiceRequest serviceRequest) {
        return super.add(serviceRequest, Hospital.class, hospitalDao);
    }
    
    @Transactional
    @ActionProcessor(action = "CHANGE")
    public ServiceResponse change(ServiceRequest serviceRequest) {
        return super.change(serviceRequest, Hospital.class, hospitalDao);
    }
    
    @Transactional
    @ActionProcessor(action = "DELETE")
    public ServiceResponse delete(ServiceRequest serviceRequest) {
        return super.delete(serviceRequest, Hospital.class, hospitalDao);
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to add passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasAddPermission(ServiceRequest serviceRequest, Hospital hospital) {
        if (serviceRequest == null || hospital == null) {
            return false;
        }
        return true;
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to fetch, change, and delete passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasPermission(ServiceRequest serviceRequest, Hospital hospital) {
        if (serviceRequest == null || hospital == null) {
            return false;
        }
        ExpressionManager<Hospital> expressionManager = new ExpressionManager<>(Hospital.class);
        applyEntityExists(expressionManager, hospital.getId());
        return hospitalDao.count(expressionManager.buildExpression()) > 0;
    }
    
    /*--------------------------------------------------------------------------
     inspects whether the entity exists with passed 'hospital' pk
     --------------------------------------------------------------------------*/
    private void applyEntityExists(ExpressionManager<Hospital> expressionManager, Integer pk) {
        if (expressionManager == null || pk == null) {
            return;
        }
        /*----------------------------------------------------------------------
         develops a new boolean-expression to validate 'id' as the
         primary-key
         ---------------------------------------------------------------------*/
        expressionManager.addExpression(idIsOfHospital(pk));
    }

    /*--------------------------------------------------------------------------
    applies further filtering for non-staff users
    --------------------------------------------------------------------------*/
    @Override
    protected void applyNonStaffFilter(ExpressionManager<Hospital> expressionManager, ServiceRequest serviceRequest) {
        // no further filtering applied
    }
    
}