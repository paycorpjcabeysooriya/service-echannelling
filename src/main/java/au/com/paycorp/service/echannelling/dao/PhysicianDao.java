package au.com.paycorp.service.echannelling.dao;

import au.com.paycorp.service.interfac.dao.IServiceDao;
import au.com.paycorp.service.echannelling.entity.Physician;
import org.springframework.stereotype.Repository;

@Repository
public interface PhysicianDao extends IServiceDao<Physician, Integer> {
}