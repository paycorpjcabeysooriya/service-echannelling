package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.interfac.service.BaseService;
import au.com.paycorp.service.interfac.dao.IServiceDao;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServiceResponse;
import au.com.paycorp.service.interfac.domain.ServicePageRequest;
import au.com.paycorp.service.interfac.base.ExpressionManager;
import au.com.paycorp.service.interfac.helper.JpaHelper;

import org.springframework.data.domain.Persistable;
import org.springframework.data.domain.Page;
import org.apache.log4j.Logger;
import java.io.Serializable;

public abstract class AbstractService<ID extends Serializable, E extends Persistable<ID>, D extends IServiceDao<E, ID>> extends BaseService {
    private static final Logger log = Logger.getLogger(AbstractService.class);
    
    protected AbstractService() {
    }
    
    protected ServiceResponse list(ServiceRequest serviceRequest, Class<E> clazz, D dao) {
        if (serviceRequest == null || clazz == null || dao == null) {
            throw new IllegalArgumentException("One of the passed argument carries a null reference. Non of the arguments can be null.");
        }
        ServicePageRequest servicePageRequest = serviceRequest.getServiceData(ServicePageRequest.class);
        log.info("[LIST]\tRequest:\t" + (servicePageRequest == null ? "Request not found!" : servicePageRequest));
        Page<E> page = null;
        if (servicePageRequest != null) {
            ExpressionManager<E> expressionManager = new ExpressionManager<>(clazz);
            super.applyLikeFilter(expressionManager, servicePageRequest);
            applyNonStaffFilter(expressionManager, serviceRequest);
            page = dao.findAll(expressionManager.buildExpression(), JpaHelper.toJpaPageRequest(servicePageRequest));
        }
        log.info("[LIST]\tResponse:\t" + (page == null ? "Page not found!" : page));
        return super.buildServiceResponse(serviceRequest, JpaHelper.toServicePageResponse(page, servicePageRequest), true);
    }
    
    protected ServiceResponse fetch(ServiceRequest serviceRequest, Class<E> clazz, D dao) {
        if (serviceRequest == null || clazz == null || dao == null) {
            throw new IllegalArgumentException("One of the passed argument carries a null reference. Non of the arguments can be null.");
        }
        E requestedEntity = serviceRequest.getServiceData(clazz);
        log.info("[FETCH]\tRequest:\t" + (requestedEntity == null ? "Request not found!" : requestedEntity));
        E fetchedEntity = null;
        if (requestedEntity != null && hasPermission(serviceRequest, requestedEntity)) {
            fetchedEntity = dao.getOne(requestedEntity.getId());
            log.info("[FETCH]\tStatus:\t\tSuccessfull");
        } else {
            log.info("[FETCH]\tStatus:\t\tFailed");
        }
        log.info("[FETCH]\tResponse:\t" + (fetchedEntity == null ? "Entity not found!" : fetchedEntity));
        return super.buildServiceResponse(serviceRequest, fetchedEntity);
    }
    
    protected ServiceResponse add(ServiceRequest serviceRequest, Class<E> clazz, D dao) {
        if (serviceRequest == null || clazz == null || dao == null) {
            throw new IllegalArgumentException("One of the passed argument carries a null reference. Non of the arguments can be null.");
        }
        E requestedEntity = serviceRequest.getServiceData(clazz);
        log.info("[ADD]\tRequest:\t" + (requestedEntity == null ? "Request not found!" : requestedEntity));
        E addedEntity = null;
        if (requestedEntity != null && hasAddPermission(serviceRequest, requestedEntity)) {
            addedEntity = dao.save(requestedEntity);
            log.info("[ADD]\tStatus:\t\tSuccessfull");
        } else {
            log.info("[ADD]\tStatus:\t\tFailed");
        }
        log.info("[ADD]\tResponse:\t" + (addedEntity == null ? "Entity not found!" : addedEntity));
        return super.buildServiceResponse(serviceRequest, addedEntity);
    }
    
    protected ServiceResponse change(ServiceRequest serviceRequest, Class<E> clazz, D dao) {
        if (serviceRequest == null || clazz == null || dao == null) {
            throw new IllegalArgumentException("One of the passed argument carries a null reference. Non of the arguments can be null.");
        }
        E requestedEntity = serviceRequest.getServiceData(clazz);
        log.info("[CHANGE]\tRequest:\t" + (requestedEntity == null ? "Request not found!" : requestedEntity));
        E changedEntity = null;
        if (requestedEntity != null && hasPermission(serviceRequest, requestedEntity)) {
            changedEntity = dao.save(requestedEntity);
            log.info("[CHANGE]\tStatus:\t\tSuccessfull");
        } else {
            log.info("[CHANGE]\tStatus:\t\tFailed");
        }
        log.info("[CHANGE]\tResponse:\t" + (changedEntity == null ? "Entity not found!" : changedEntity));
        return super.buildServiceResponse(serviceRequest, changedEntity);
    }
    
    protected ServiceResponse delete(ServiceRequest serviceRequest, Class<E> clazz, D dao) {
        if (serviceRequest == null || clazz == null || dao == null) {
            throw new IllegalArgumentException("One of the passed argument carries a null reference. Non of the arguments can be null.");
        }
        E requestedEntity = serviceRequest.getServiceData(clazz);
        log.info("[DELETE]\tRequest:\t" + (requestedEntity == null ? "Request not found!" : requestedEntity));
        Boolean isDeleted = false;
        if (requestedEntity != null && hasPermission(serviceRequest, requestedEntity)) {
            dao.delete(requestedEntity.getId());
            isDeleted = true;
            log.info("[DELETE]\tStatus:\t\tSuccessfull");
        } else {
            log.info("[DELETE]\tStatus:\t\tFailed");
        }
        return super.buildServiceResponse(serviceRequest, isDeleted);
    }
    
    protected abstract boolean hasAddPermission(ServiceRequest serviceRequest, E entity);
    
    protected abstract boolean hasPermission(ServiceRequest serviceRequest, E entity);
    
    protected abstract void applyNonStaffFilter(ExpressionManager<E> expressionManager, ServiceRequest serviceRequest);
    
}