package au.com.paycorp.service.echannelling.dao;

import au.com.paycorp.service.interfac.dao.IServiceDao;
import au.com.paycorp.service.echannelling.entity.Channelling;
import au.com.paycorp.service.echannelling.entity.ChannellingPK;
import org.springframework.stereotype.Repository;

@Repository
public interface ChannellingDao extends IServiceDao<Channelling, ChannellingPK> {
}