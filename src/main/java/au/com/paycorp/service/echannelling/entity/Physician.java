package au.com.paycorp.service.echannelling.entity;

import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.google.gson.GsonBuilder;

@Entity
@Table(name = "physician")
public class Physician extends AbstractEntity<Integer> {
    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @Column(name = "title")
    private String title;
    
    @Basic(optional = false)
    @Column(name = "surname")
    private String surname;
    
    @Basic(optional = false)
    @Column(name = "othernames")
    private String othernames;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "physician")
    private Collection<Channelling> channellingCollection;
    
    @JoinColumn(name = "specialty_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Specialty specialtyId;

    public Physician() {
    }
    
    public Physician(String title, String surname, String othernames, Specialty specialtyId) {
        this.title = title;
        this.surname = surname;
        this.othernames = othernames;
        this.specialtyId = specialtyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOthernames() {
        return othernames;
    }

    public void setOthernames(String othernames) {
        this.othernames = othernames;
    }

    public Collection<Channelling> getChannellingCollection() {
        return channellingCollection;
    }

    public void setChannellingCollection(Collection<Channelling> channellingCollection) {
        this.channellingCollection = channellingCollection;
    }

    public Specialty getSpecialtyId() {
        return specialtyId;
    }

    public void setSpecialtyId(Specialty specialtyId) {
        this.specialtyId = specialtyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Physician)) {
            return false;
        }
        Physician other = (Physician) object;
        if ((getId() == null && other.getId() != null) || (getId() != null && !getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }
    
}