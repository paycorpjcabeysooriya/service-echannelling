package au.com.paycorp.service.echannelling.entity;

import org.springframework.data.jpa.domain.AbstractPersistable;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public class AbstractEntity<PK extends Serializable> extends AbstractPersistable<PK> {
    private static final long serialVersionUID = 1L;
            
    public AbstractEntity() {
    }
    
    @Override
    public void setId(PK id) {
        super.setId(id);
    }
}