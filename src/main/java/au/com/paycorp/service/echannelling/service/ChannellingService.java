package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Channelling;
import au.com.paycorp.service.echannelling.entity.ChannellingPK;
import au.com.paycorp.service.echannelling.dao.ChannellingDao;
import static au.com.paycorp.service.echannelling.service.helper.Expression.hospitalIdIsOfChannelling;
import static au.com.paycorp.service.echannelling.service.helper.Expression.physicianIdIsOfChannelling;
import static au.com.paycorp.service.echannelling.service.helper.Expression.startatIsOfChannelling;

import au.com.paycorp.service.interfac.base.annotation.Processor;
import au.com.paycorp.service.interfac.base.annotation.ActionProcessor;
import au.com.paycorp.service.interfac.base.ExpressionManager;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServiceResponse;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.types.expr.BooleanExpression;
import java.util.List;
import java.util.ArrayList;

@Service
@Processor (resource = "CHANNELLING_SERVICE")
public class ChannellingService extends AbstractService<ChannellingPK, Channelling, ChannellingDao> {
    
    @Autowired
    private ChannellingDao channellingDao;
    
    public ChannellingService() {
    }
    
    @Transactional
    @ActionProcessor(action = "LIST")
    public ServiceResponse list(ServiceRequest serviceRequest) {
        return super.list(serviceRequest, Channelling.class, channellingDao);
    }
    
    @Transactional
    @ActionProcessor(action = "FETCH")
    public ServiceResponse fetch(ServiceRequest serviceRequest) {
        return super.fetch(serviceRequest, Channelling.class, channellingDao);
    }
    
    @Transactional
    @ActionProcessor(action = "ADD")
    public ServiceResponse add(ServiceRequest serviceRequest) {
        return super.add(serviceRequest, Channelling.class, channellingDao);
    }
    
    @Transactional
    @ActionProcessor(action = "CHANGE")
    public ServiceResponse change(ServiceRequest serviceRequest) {
        return super.change(serviceRequest, Channelling.class, channellingDao);
    }
    
    @Transactional
    @ActionProcessor(action = "DELETE")
    public ServiceResponse delete(ServiceRequest serviceRequest) {
        return super.delete(serviceRequest, Channelling.class, channellingDao);
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to add passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasAddPermission(ServiceRequest serviceRequest, Channelling channelling) {
        if (serviceRequest == null || channelling == null) {
            return false;
        }
        return true;
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to fetch, change, and delete passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasPermission(ServiceRequest serviceRequest, Channelling channelling) {
        if (serviceRequest == null || channelling == null) {
            return false;
        }
        ExpressionManager<Channelling> expressionManager = new ExpressionManager<>(Channelling.class);
        applyEntityExists(expressionManager, channelling.getId());
        return channellingDao.count(expressionManager.buildExpression()) > 0;
    }
    
    /*--------------------------------------------------------------------------
     inspects whether the entity exists with passed 'channelling' pk
     --------------------------------------------------------------------------*/
    private void applyEntityExists(ExpressionManager<Channelling> expressionManager, ChannellingPK pk) {
        if (expressionManager == null || pk == null) {
            return;
        }
        /*----------------------------------------------------------------------
        develops a new boolean-expression to validate 'ChannellingPK' as the
        primary-key
        ----------------------------------------------------------------------*/
        List<BooleanExpression> booleanExpressions = new ArrayList<>();
        booleanExpressions.add(hospitalIdIsOfChannelling(pk.getHospitalId()));
        booleanExpressions.add(physicianIdIsOfChannelling(pk.getPhysicianId()));
        booleanExpressions.add(startatIsOfChannelling(pk.getStartat()));
        BooleanExpression expression = BooleanExpression.allOf(booleanExpressions.toArray(new BooleanExpression[]{}));
        expressionManager.addExpression(expression);
    }

    /*--------------------------------------------------------------------------
    applies further filtering for non-staff users
    --------------------------------------------------------------------------*/
    @Override
    protected void applyNonStaffFilter(ExpressionManager<Channelling> expressionManager, ServiceRequest serviceRequest) {
        // no further filtering applied
    }
    
}