package au.com.paycorp.service.echannelling.service.helper;

import au.com.paycorp.service.echannelling.entity.QSpecialty;
import au.com.paycorp.service.echannelling.entity.QPhysician;
import au.com.paycorp.service.echannelling.entity.QPatient;
import au.com.paycorp.service.echannelling.entity.QHospital;
import au.com.paycorp.service.echannelling.entity.QChannelling;

import com.mysema.query.types.expr.BooleanExpression;
import java.util.Date;

public class Expression {

    public static BooleanExpression idIsOfSpecialty(Integer id) {
        return QSpecialty.specialty.id.eq(id);
    }
    
    public static BooleanExpression idIsOfPhysician(Integer id) {
        return QPhysician.physician.id.eq(id);
    }
    
    public static BooleanExpression idIsOfPatient(Integer id) {
        return QPatient.patient.id.eq(id);
    }
    
    public static BooleanExpression idIsOfHospital(Integer id) {
        return QHospital.hospital.id.eq(id);
    }
    
    public static BooleanExpression hospitalIdIsOfChannelling(Integer id) {
        return QChannelling.channelling.channellingPK().hospitalId.eq(id);
    }
    
    public static BooleanExpression physicianIdIsOfChannelling(Integer id) {
        return QChannelling.channelling.channellingPK().physicianId.eq(id);
    }
    
    public static BooleanExpression startatIsOfChannelling(Date startat) {
        return QChannelling.channelling.channellingPK().startat.eq(startat);
    }

}