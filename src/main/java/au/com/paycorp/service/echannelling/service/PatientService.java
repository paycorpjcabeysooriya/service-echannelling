package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Patient;
import au.com.paycorp.service.echannelling.dao.PatientDao;
import static au.com.paycorp.service.echannelling.service.helper.Expression.idIsOfPatient;

import au.com.paycorp.service.interfac.base.annotation.Processor;
import au.com.paycorp.service.interfac.base.annotation.ActionProcessor;
import au.com.paycorp.service.interfac.base.ExpressionManager;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServiceResponse;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service
@Processor (resource = "PATIENT_SERVICE")
public class PatientService extends AbstractService<Integer, Patient, PatientDao> {
    
    @Autowired
    private PatientDao patientDao;
    
    public PatientService() {
    }
    
    @Transactional
    @ActionProcessor(action = "LIST")
    public ServiceResponse list(ServiceRequest serviceRequest) {
        return super.list(serviceRequest, Patient.class, patientDao);
    }
    
    @Transactional
    @ActionProcessor(action = "FETCH")
    public ServiceResponse fetch(ServiceRequest serviceRequest) {
        return super.fetch(serviceRequest, Patient.class, patientDao);
    }
    
    @Transactional
    @ActionProcessor(action = "ADD")
    public ServiceResponse add(ServiceRequest serviceRequest) {
        return super.add(serviceRequest, Patient.class, patientDao);
    }
    
    @Transactional
    @ActionProcessor(action = "CHANGE")
    public ServiceResponse change(ServiceRequest serviceRequest) {
        return super.change(serviceRequest, Patient.class, patientDao);
    }
    
    @Transactional
    @ActionProcessor(action = "DELETE")
    public ServiceResponse delete(ServiceRequest serviceRequest) {
        return super.delete(serviceRequest, Patient.class, patientDao);
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to add passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasAddPermission(ServiceRequest serviceRequest, Patient patient) {
        if (serviceRequest == null || patient == null) {
            return false;
        }
        return true;
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to fetch, change, and delete passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasPermission(ServiceRequest serviceRequest, Patient patient) {
        if (serviceRequest == null || patient == null) {
            return false;
        }
        ExpressionManager<Patient> expressionManager = new ExpressionManager<>(Patient.class);
        applyEntityExists(expressionManager, patient.getId());
        return patientDao.count(expressionManager.buildExpression()) > 0;
    }
    
    /*--------------------------------------------------------------------------
     inspects whether the entity exists with passed 'patient' pk
     --------------------------------------------------------------------------*/
    private void applyEntityExists(ExpressionManager<Patient> expressionManager, Integer pk) {
        if (expressionManager == null || pk == null) {
            return;
        }
        /*----------------------------------------------------------------------
         develops a new boolean-expression to validate 'id' as the
         primary-key
         ---------------------------------------------------------------------*/
        expressionManager.addExpression(idIsOfPatient(pk));
    }

    /*--------------------------------------------------------------------------
    applies further filtering for non-staff users
    --------------------------------------------------------------------------*/
    @Override
    protected void applyNonStaffFilter(ExpressionManager<Patient> expressionManager, ServiceRequest serviceRequest) {
        // no further filtering applied
    }
    
}