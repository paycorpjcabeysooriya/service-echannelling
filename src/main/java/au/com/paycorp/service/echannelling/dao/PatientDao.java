package au.com.paycorp.service.echannelling.dao;

import au.com.paycorp.service.interfac.dao.IServiceDao;
import au.com.paycorp.service.echannelling.entity.Patient;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientDao extends IServiceDao<Patient, Integer> {
}