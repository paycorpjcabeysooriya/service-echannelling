package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Specialty;
import au.com.paycorp.service.echannelling.dao.SpecialtyDao;
import static au.com.paycorp.service.echannelling.service.helper.Expression.idIsOfSpecialty;

import au.com.paycorp.service.interfac.base.annotation.Processor;
import au.com.paycorp.service.interfac.base.annotation.ActionProcessor;
import au.com.paycorp.service.interfac.base.ExpressionManager;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServiceResponse;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service
@Processor (resource = "SPECIALTY_SERVICE")
public class SpecialtyService extends AbstractService<Integer, Specialty, SpecialtyDao> {
    
    @Autowired
    private SpecialtyDao specialtyDao;
    
    public SpecialtyService() {
    }
    
    @Transactional
    @ActionProcessor(action = "LIST")
    public ServiceResponse list(ServiceRequest serviceRequest) {
        return super.list(serviceRequest, Specialty.class, specialtyDao);
    }
    
    @Transactional
    @ActionProcessor(action = "FETCH")
    public ServiceResponse fetch(ServiceRequest serviceRequest) {
        return super.fetch(serviceRequest, Specialty.class, specialtyDao);
    }
    
    @Transactional
    @ActionProcessor(action = "ADD")
    public ServiceResponse add(ServiceRequest serviceRequest) {
        return super.add(serviceRequest, Specialty.class, specialtyDao);
    }
    
    @Transactional
    @ActionProcessor(action = "CHANGE")
    public ServiceResponse change(ServiceRequest serviceRequest) {
        return super.change(serviceRequest, Specialty.class, specialtyDao);
    }
    
    @Transactional
    @ActionProcessor(action = "DELETE")
    public ServiceResponse delete(ServiceRequest serviceRequest) {
        return super.delete(serviceRequest, Specialty.class, specialtyDao);
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to add passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasAddPermission(ServiceRequest serviceRequest, Specialty specialty) {
        if (serviceRequest == null || specialty == null) {
            return false;
        }
        return true;
    }
    
    /*--------------------------------------------------------------------------
    checks the user has permission to fetch, change, and delete passed entity
    --------------------------------------------------------------------------*/
    @Override
    protected boolean hasPermission(ServiceRequest serviceRequest, Specialty specialty) {
        if (serviceRequest == null || specialty == null) {
            return false;
        }
        ExpressionManager<Specialty> expressionManager = new ExpressionManager<>(Specialty.class);
        applyEntityExists(expressionManager, specialty.getId());
        return specialtyDao.count(expressionManager.buildExpression()) > 0;
    }
    
    /*--------------------------------------------------------------------------
     inspects whether the entity exists with passed 'specialty' pk
     --------------------------------------------------------------------------*/
    private void applyEntityExists(ExpressionManager<Specialty> expressionManager, Integer pk) {
        if (expressionManager == null || pk == null) {
            return;
        }
        /*----------------------------------------------------------------------
         develops a new boolean-expression to validate 'id' as the
         primary-key
         ---------------------------------------------------------------------*/
        expressionManager.addExpression(idIsOfSpecialty(pk));
    }

    /*--------------------------------------------------------------------------
    applies further filtering for non-staff users
    --------------------------------------------------------------------------*/
    @Override
    protected void applyNonStaffFilter(ExpressionManager<Specialty> expressionManager, ServiceRequest serviceRequest) {
        // no further filtering applied
    }
    
}