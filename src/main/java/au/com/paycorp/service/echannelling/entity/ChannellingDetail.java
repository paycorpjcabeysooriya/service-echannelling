package au.com.paycorp.service.echannelling.entity;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.domain.Persistable;
import com.google.gson.GsonBuilder;

@Entity
@Table(name = "channelling_detail")
public class ChannellingDetail implements Persistable<ChannellingDetailPK> {
    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected ChannellingDetailPK channellingDetailPK;
    @Basic(optional = false)
    @Column(name = "checkin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkin;
    
    @Basic(optional = false)
    @Column(name = "checkout")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkout;
    
    @Basic(optional = false)
    @Column(name = "status")
    private Character status;
    
    @JoinColumns({
        @JoinColumn(name = "hospital_id", referencedColumnName = "hospital_id", insertable = false, updatable = false),
        @JoinColumn(name = "physician_id", referencedColumnName = "physician_id", insertable = false, updatable = false),
        @JoinColumn(name = "startat", referencedColumnName = "startat", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Channelling channelling;

    public ChannellingDetail() {
    }
    
    @Override
    public ChannellingDetailPK getId() {
        return channellingDetailPK;
    }
    
    @Override
    public boolean isNew() {
        return channellingDetailPK == null;
    }

    public ChannellingDetail(ChannellingDetailPK channellingDetailPK) {
        this.channellingDetailPK = channellingDetailPK;
    }

    public ChannellingDetail(ChannellingDetailPK channellingDetailPK, Date checkin, Date checkout, Character status) {
        this.channellingDetailPK = channellingDetailPK;
        this.checkin = checkin;
        this.checkout = checkout;
        this.status = status;
    }

    public ChannellingDetail(int hospitalId, int physicianId, Date startat, int patientId) {
        this.channellingDetailPK = new ChannellingDetailPK(hospitalId, physicianId, startat, patientId);
    }

    public ChannellingDetailPK getChannellingDetailPK() {
        return channellingDetailPK;
    }

    public void setChannellingDetailPK(ChannellingDetailPK channellingDetailPK) {
        this.channellingDetailPK = channellingDetailPK;
    }

    public Date getCheckin() {
        return checkin;
    }

    public void setCheckin(Date checkin) {
        this.checkin = checkin;
    }

    public Date getCheckout() {
        return checkout;
    }

    public void setCheckout(Date checkout) {
        this.checkout = checkout;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Channelling getChannelling() {
        return channelling;
    }

    public void setChannelling(Channelling channelling) {
        this.channelling = channelling;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (channellingDetailPK != null ? channellingDetailPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ChannellingDetail)) {
            return false;
        }
        ChannellingDetail other = (ChannellingDetail) object;
        if ((this.channellingDetailPK == null && other.channellingDetailPK != null) || (this.channellingDetailPK != null && !this.channellingDetailPK.equals(other.channellingDetailPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }
    
}