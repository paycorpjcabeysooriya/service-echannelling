package au.com.paycorp.service.echannelling.entity;

import java.util.Date;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.domain.Persistable;
import com.google.gson.GsonBuilder;

@Entity
@Table(name = "channelling")
public class Channelling implements Persistable<ChannellingPK> {
    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected ChannellingPK channellingPK;
    
    @Basic(optional = false)
    @Column(name = "endat")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endat;
    
    @Basic(optional = false)
    @Column(name = "status")
    private Character status;
    
    @JoinColumn(name = "physician_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Physician physician;
    
    @JoinColumn(name = "hospital_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Hospital hospital;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "channelling")
    private Collection<ChannellingDetail> channellingDetailCollection;

    public Channelling() {
    }
    
    @Override
    public ChannellingPK getId() {
        return channellingPK;
    }
    
    @Override
    public boolean isNew() {
        return channellingPK == null;
    }

    public ChannellingPK getChannellingPK() {
        return channellingPK;
    }

    public void setChannellingPK(ChannellingPK channellingPK) {
        this.channellingPK = channellingPK;
    }

    public Date getEndat() {
        return endat;
    }

    public void setEndat(Date endat) {
        this.endat = endat;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Physician getPhysician() {
        return physician;
    }

    public void setPhysician(Physician physician) {
        this.physician = physician;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Collection<ChannellingDetail> getChannellingDetailCollection() {
        return channellingDetailCollection;
    }

    public void setChannellingDetailCollection(Collection<ChannellingDetail> channellingDetailCollection) {
        this.channellingDetailCollection = channellingDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (channellingPK != null ? channellingPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Channelling)) {
            return false;
        }
        Channelling other = (Channelling) object;
        if ((this.channellingPK == null && other.channellingPK != null) || (this.channellingPK != null && !this.channellingPK.equals(other.channellingPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }
    
}
