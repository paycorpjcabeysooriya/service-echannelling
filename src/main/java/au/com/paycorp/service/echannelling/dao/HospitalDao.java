package au.com.paycorp.service.echannelling.dao;

import au.com.paycorp.service.interfac.dao.IServiceDao;
import au.com.paycorp.service.echannelling.entity.Hospital;
import org.springframework.stereotype.Repository;

@Repository
public interface HospitalDao extends IServiceDao<Hospital, Integer> {
}