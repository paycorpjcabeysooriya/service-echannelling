package au.com.paycorp.service.echannelling.dao;

import au.com.paycorp.service.interfac.dao.IServiceDao;
import au.com.paycorp.service.echannelling.entity.Specialty;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialtyDao extends IServiceDao<Specialty, Integer> {
}