package au.com.paycorp.service.echannelling.entity;

import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.google.gson.GsonBuilder;

@Entity
@Table(name = "hospital")
public class Hospital extends AbstractEntity<Integer> {
    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @Column(name = "location")
    private String location;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospital")
    private Collection<Channelling> channellingCollection;

    public Hospital() {
    }
    
    public Hospital(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Collection<Channelling> getChannellingCollection() {
        return channellingCollection;
    }

    public void setChannellingCollection(Collection<Channelling> channellingCollection) {
        this.channellingCollection = channellingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Hospital)) {
            return false;
        }
        Hospital other = (Hospital) object;
        if ((getId() == null && other.getId() != null) || (getId() != null && !getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }
    
}
