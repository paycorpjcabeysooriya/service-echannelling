package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Hospital;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServicePageRequest;
import au.com.paycorp.service.interfac.domain.ServicePageResponse;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.apache.log4j.Logger;
import com.google.common.collect.Lists;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApplicationContext.xml"})
@Transactional
public class HospitalServiceUT {
    
    @Autowired
    private HospitalService hospitalService;
    private static final Logger log = Logger.getLogger(HospitalServiceUT.class);
    private static final ServiceRequest staffServiceRequest = new ServiceRequest();
    private static final ServiceRequest nonStaffServiceRequest = new ServiceRequest();
    static {
        staffServiceRequest.setRequestId("12345");
        staffServiceRequest.setResource("HOSPITAL_SERVICE");
        staffServiceRequest.setStaff(true);
        staffServiceRequest.setCustomerId(50000001);
        
        nonStaffServiceRequest.setRequestId("12345");
        nonStaffServiceRequest.setResource("HOSPITAL_SERVICE");
        nonStaffServiceRequest.setStaff(false);
        nonStaffServiceRequest.setCustomerId(50000273);
        nonStaffServiceRequest.setClients(Lists.newArrayList(10000005, 10003143, 10000167, 10004172, 10000970));
    }
    
    //@Test
    public void testAddStaff() {
        staffServiceRequest.setAction("ADD");
        Hospital hospital = new Hospital("test.name", "test.location");
        staffServiceRequest.setServiceData(hospital);
        
        hospital = hospitalService.add(staffServiceRequest).getServiceData(Hospital.class);
        if (hospital != null) {
            log.info("*** TEST - testAddStaff - RETURN OK - [" + hospital + "]");
        } else {
            log.info("*** TEST - testAddStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testAddNonStaff() {
        nonStaffServiceRequest.setAction("ADD");
        Hospital hospital = new Hospital("test.name", "test.location");
        nonStaffServiceRequest.setServiceData(hospital);
        
        hospital = hospitalService.add(nonStaffServiceRequest).getServiceData(Hospital.class);
        if (hospital != null) {
            log.info("*** TEST - testAddNonStaff - RETURN OK - [" + hospital + "]");
        } else {
            log.info("*** TEST - testAddNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testChangeStaff() {
        staffServiceRequest.setAction("CHANGE");
        Hospital hospital = new Hospital("test.name", "test.location");
        hospital.setId(1);
        staffServiceRequest.setServiceData(hospital);
        
        hospital = hospitalService.change(staffServiceRequest).getServiceData(Hospital.class);
        if (hospital != null) {
            log.info("*** TEST - testChangeStaff - RETURN OK - [" +  hospital + "]");
        } else {
            log.info("*** TEST - testChangeStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testChangeNonStaff() {
        nonStaffServiceRequest.setAction("CHANGE");
        Hospital hospital = new Hospital("test.name", "test.location");
        hospital.setId(1);
        nonStaffServiceRequest.setServiceData(hospital);
        
        hospital = hospitalService.change(nonStaffServiceRequest).getServiceData(Hospital.class);
        if (hospital != null) {
            log.info("*** TEST - testChangeNonStaff - RETURN OK - [" + hospital + "]");
        } else {
            log.info("*** TEST - testChangeNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testFetchStaff() {
        staffServiceRequest.setAction("FETCH");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to fech, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Hospital hospital = new Hospital();
        hospital.setId(1);
        staffServiceRequest.setServiceData(hospital);
        
        hospital = hospitalService.fetch(staffServiceRequest).getServiceData(Hospital.class);
        if (hospital != null) {
            log.info("*** TEST - testFetchStaff - RETURN OK - [" + hospital + "]");
        } else {
            log.info("*** TEST - testFetchStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testFetchNonStaff() {
        nonStaffServiceRequest.setAction("FETCH");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to fech, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Hospital hospital = new Hospital();
        hospital.setId(1);
        nonStaffServiceRequest.setServiceData(hospital);
        
        hospital = hospitalService.fetch(nonStaffServiceRequest).getServiceData(Hospital.class);
        if (hospital != null) {
            log.info("*** TEST - testFetchNonStaff - RETURN OK - [" + hospital + "]");
        } else {
            log.info("*** TEST - testFetchNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testDeleteStaff() {
        staffServiceRequest.setAction("DELETE");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to delete, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Hospital hospital = new Hospital();
        hospital.setId(1);
        staffServiceRequest.setServiceData(hospital);
        
        Boolean isDeleted = hospitalService.delete(staffServiceRequest).getServiceData(Boolean.class);
        if (isDeleted) {
            log.info("*** TEST - testDeleteStaff - DELETE OK - [" + hospital + "]");
        } else {
            log.info("*** TEST - testDeleteStaff - DELETE FAIL");
        }
        // PASS
    }
    
    //@Test
    public void testDeleteNonStaff() {
        nonStaffServiceRequest.setAction("DELETE");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to delete, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Hospital hospital = new Hospital();
        hospital.setId(1);
        nonStaffServiceRequest.setServiceData(hospital);
        
        Boolean isDeleted = hospitalService.delete(nonStaffServiceRequest).getServiceData(Boolean.class);
        if (isDeleted) {
            log.info("*** TEST - testDeleteNonStaff - DELETE OK - ["  + hospital + "]");
        } else {
            log.info("*** TEST - testDeleteNonStaff - DELETE FAIL");
        }
        // PASS
    }
    
    //@Test
    public void testListStaff() {
        staffServiceRequest.setAction("LIST");
        ServicePageRequest servicePageRequest = new ServicePageRequest();
        staffServiceRequest.setServiceData(servicePageRequest);

        JavaType type = new ObjectMapper().getTypeFactory().constructParametricType(ServicePageResponse.class, Hospital.class);
        ServicePageResponse<Hospital> queryResponse = hospitalService.list(staffServiceRequest).getServiceData(type);
        if (queryResponse != null) {
            List<Hospital> list = queryResponse.getObjects();
            log.info("*** TEST - testListNonStaff - RETURN OK - " + list);
        } else {
            log.info("*** TEST - testListStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testListNonStaff() {
        nonStaffServiceRequest.setAction("LIST");
        ServicePageRequest servicePageRequest = new ServicePageRequest();
        nonStaffServiceRequest.setServiceData(servicePageRequest);

        JavaType type = new ObjectMapper().getTypeFactory().constructParametricType(ServicePageResponse.class, Hospital.class);
        ServicePageResponse<Hospital> queryResponse = hospitalService.list(nonStaffServiceRequest).getServiceData(type);
        if (queryResponse != null) {
            List<Hospital> list = queryResponse.getObjects();
            log.info("*** TEST - testListNonStaff - RETURN OK - " + list);
        } else {
            log.info("*** TEST - testListNonStaff - RETURN NULL");
        }
        // PASS
    }
    
}