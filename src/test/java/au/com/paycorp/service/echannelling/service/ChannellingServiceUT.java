package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Channelling;
import au.com.paycorp.service.echannelling.entity.ChannellingPK;
import au.com.paycorp.service.echannelling.entity.Hospital;
import au.com.paycorp.service.echannelling.entity.Physician;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServicePageRequest;
import au.com.paycorp.service.interfac.domain.ServicePageResponse;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.google.common.collect.Lists;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.ParseException;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApplicationContext.xml"})
@Transactional
public class ChannellingServiceUT {
    
    @Autowired
    private ChannellingService channellingService;
    private static final Logger log = Logger.getLogger(HospitalServiceUT.class);
    private static final ServiceRequest staffServiceRequest = new ServiceRequest();
    private static final ServiceRequest nonStaffServiceRequest = new ServiceRequest();
    static {
        staffServiceRequest.setRequestId("12345");
        staffServiceRequest.setResource("CHANNELLING_SERVICE");
        staffServiceRequest.setStaff(true);
        staffServiceRequest.setCustomerId(50000001);
        
        nonStaffServiceRequest.setRequestId("12345");
        nonStaffServiceRequest.setResource("CHANNELLING_SERVICE");
        nonStaffServiceRequest.setStaff(false);
        nonStaffServiceRequest.setCustomerId(50000273);
        nonStaffServiceRequest.setClients(Lists.newArrayList(10000005, 10003143, 10000167, 10004172, 10000970));
    }
    
    //@Test
    public void testAddStaff() {
        staffServiceRequest.setAction("ADD");
        Hospital hospital = new Hospital();
        hospital.setId(1);
        Physician physician = new Physician();
        physician.setId(1);
        ChannellingPK channellingPK = new ChannellingPK(hospital.getId(), physician.getId(), new Date());
        Channelling channelling = new Channelling();
        channelling.setChannellingPK(channellingPK);
        channelling.setHospital(hospital);
        channelling.setPhysician(physician);
        channelling.setEndat(new DateTime(channelling.getChannellingPK().getStartat().getTime()).plusHours(2).toDate());
        channelling.setStatus('A');
        staffServiceRequest.setServiceData(channelling);
        
        channelling = channellingService.add(staffServiceRequest).getServiceData(Channelling.class);
        if (channelling != null) {
            log.info("*** TEST - testAddStaff - RETURN OK - [" + channelling + "]");
        } else {
            log.info("*** TEST - testAddStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testAddNonStaff() {
        nonStaffServiceRequest.setAction("ADD");
        Hospital hospital = new Hospital();
        hospital.setId(2);
        Physician physician = new Physician();
        physician.setId(2);
        ChannellingPK channellingPK = new ChannellingPK(hospital.getId(), physician.getId(), new Date());
        Channelling channelling = new Channelling();
        channelling.setChannellingPK(channellingPK);
        channelling.setHospital(hospital);
        channelling.setPhysician(physician);
        channelling.setEndat(new DateTime(channelling.getChannellingPK().getStartat().getTime()).plusHours(2).toDate());
        nonStaffServiceRequest.setServiceData(channelling);
        
        channelling = channellingService.add(nonStaffServiceRequest).getServiceData(Channelling.class);
        if (channelling != null) {
            log.info("*** TEST - testAddNonStaff - RETURN OK - [" + channelling + "]");
        } else {
            log.info("*** TEST - testAddNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testFetchStaff() {
        staffServiceRequest.setAction("FETCH");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to fech, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Hospital hospital = new Hospital();
        hospital.setId(2);
        Physician physician = new Physician();
        physician.setId(1);
        Date startat = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            startat = simpleDateFormat.parse("2015-02-01T14:00:00.000");
        } catch (ParseException exp) {
        }
        ChannellingPK channellingPK = new ChannellingPK(hospital.getId(), physician.getId(), startat);
        Channelling channelling = new Channelling();
        channelling.setChannellingPK(channellingPK);
        channelling.setHospital(hospital);
        channelling.setPhysician(physician);
        staffServiceRequest.setServiceData(channelling);
                
        channelling = channellingService.fetch(staffServiceRequest).getServiceData(Channelling.class);
        if (channelling != null) {
            log.info("*** TEST - testFetchStaff - RETURN OK - [" + channelling + "]");
        } else {
            log.info("*** TEST - testFetchStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testFetchNonStaff() {
        nonStaffServiceRequest.setAction("FETCH");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to fech, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Hospital hospital = new Hospital();
        hospital.setId(2);
        Physician physician = new Physician();
        physician.setId(1);
        Date startat = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            startat = simpleDateFormat.parse("2015-02-01T14:00:00.000");
        } catch (ParseException exp) {
        }
        ChannellingPK channellingPK = new ChannellingPK(hospital.getId(), physician.getId(), startat);
        Channelling channelling = new Channelling();
        channelling.setChannellingPK(channellingPK);
        nonStaffServiceRequest.setServiceData(channelling);
        
        channelling = channellingService.fetch(nonStaffServiceRequest).getServiceData(Channelling.class);
        if (channelling != null) {
            log.info("*** TEST - testFetchNonStaff - RETURN OK - [" + channelling + "]");
        } else {
            log.info("*** TEST - testFetchNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testListStaff() {
        staffServiceRequest.setAction("LIST");
        ServicePageRequest servicePageRequest = new ServicePageRequest();
        staffServiceRequest.setServiceData(servicePageRequest);

        JavaType type = new ObjectMapper().getTypeFactory().constructParametricType(ServicePageResponse.class, Hospital.class);
        ServicePageResponse<Hospital> queryResponse = channellingService.list(staffServiceRequest).getServiceData(type);
        if (queryResponse != null) {
            List<Hospital> list = queryResponse.getObjects();
            log.info("*** TEST - testListNonStaff - RETURN OK - " + list);
        } else {
            log.info("*** TEST - testListStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testListNonStaff() {
        nonStaffServiceRequest.setAction("LIST");
        ServicePageRequest servicePageRequest = new ServicePageRequest();
        nonStaffServiceRequest.setServiceData(servicePageRequest);

        JavaType type = new ObjectMapper().getTypeFactory().constructParametricType(ServicePageResponse.class, Hospital.class);
        ServicePageResponse<Hospital> queryResponse = channellingService.list(nonStaffServiceRequest).getServiceData(type);
        if (queryResponse != null) {
            List<Hospital> list = queryResponse.getObjects();
            log.info("*** TEST - testListNonStaff - RETURN OK - " + list);
        } else {
            log.info("*** TEST - testListNonStaff - RETURN NULL");
        }
        // PASS
    }
    
}
