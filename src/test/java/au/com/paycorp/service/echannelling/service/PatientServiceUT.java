package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Patient;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServicePageRequest;
import au.com.paycorp.service.interfac.domain.ServicePageResponse;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.apache.log4j.Logger;
import com.google.common.collect.Lists;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApplicationContext.xml"})
@Transactional
public class PatientServiceUT {
    
    @Autowired
    private PatientService patientService;
    private static final Logger log = Logger.getLogger(PatientServiceUT.class);
    private static final ServiceRequest staffServiceRequest = new ServiceRequest();
    private static final ServiceRequest nonStaffServiceRequest = new ServiceRequest();
    static {
        staffServiceRequest.setRequestId("12345");
        staffServiceRequest.setResource("PATIENT_SERVICE");
        staffServiceRequest.setStaff(true);
        staffServiceRequest.setCustomerId(50000001);
        
        nonStaffServiceRequest.setRequestId("12345");
        nonStaffServiceRequest.setResource("PATIENT_SERVICE");
        nonStaffServiceRequest.setStaff(false);
        nonStaffServiceRequest.setCustomerId(50000273);
        nonStaffServiceRequest.setClients(Lists.newArrayList(10000005, 10003143, 10000167, 10004172, 10000970));
    }
    
    //@Test
    public void testAddStaff() {
        staffServiceRequest.setAction("ADD");
        Patient patient = new Patient("test.title", "test.surname", "test.othernames", 'M', new java.util.Date(), "test.address", "test.town", "test.phone", "test.email");
        staffServiceRequest.setServiceData(patient);
        
        patient = patientService.add(staffServiceRequest).getServiceData(Patient.class);
        if (patient != null) {
            log.info("*** TEST - testAddStaff - RETURN OK - [" + patient + "]");
        } else {
            log.info("*** TEST - testAddStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testAddNonStaff() {
        nonStaffServiceRequest.setAction("ADD");
        Patient patient = new Patient("test.title", "test.surname", "test.othernames", 'M', new java.util.Date(), "test.address", "test.town", "test.phone", "test.email");
        nonStaffServiceRequest.setServiceData(patient);
        
        patient = patientService.add(nonStaffServiceRequest).getServiceData(Patient.class);
        if (patient != null) {
            log.info("*** TEST - testAddNonStaff - RETURN OK - [" + patient + "]");
        } else {
            log.info("*** TEST - testAddNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testChangeStaff() {
        staffServiceRequest.setAction("CHANGE");
        Patient patient = new Patient("test.title", "test.surname", "test.othernames", 'M', new java.util.Date(), "test.address", "test.town", "test.phone", "test.email");
        patient.setId(1);
        staffServiceRequest.setServiceData(patient);
        
        patient = patientService.change(staffServiceRequest).getServiceData(Patient.class);
        if (patient != null) {
            log.info("*** TEST - testChangeStaff - RETURN OK - [" +  patient + "]");
        } else {
            log.info("*** TEST - testChangeStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testChangeNonStaff() {
        nonStaffServiceRequest.setAction("CHANGE");
        Patient patient = new Patient("test.title", "test.surname", "test.othernames", 'M', new java.util.Date(), "test.address", "test.town", "test.phone", "test.email");
        patient.setId(1);
        nonStaffServiceRequest.setServiceData(patient);
        
        patient = patientService.change(nonStaffServiceRequest).getServiceData(Patient.class);
        if (patient != null) {
            log.info("*** TEST - testChangeNonStaff - RETURN OK - [" + patient + "]");
        } else {
            log.info("*** TEST - testChangeNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testFetchStaff() {
        staffServiceRequest.setAction("FETCH");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to fech, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Patient patient = new Patient();
        patient.setId(1);
        staffServiceRequest.setServiceData(patient);
        
        patient = patientService.fetch(staffServiceRequest).getServiceData(Patient.class);
        if (patient != null) {
            log.info("*** TEST - testFetchStaff - RETURN OK - [" + patient + "]");
        } else {
            log.info("*** TEST - testFetchStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testFetchNonStaff() {
        nonStaffServiceRequest.setAction("FETCH");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to fech, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Patient patient = new Patient();
        patient.setId(1);
        nonStaffServiceRequest.setServiceData(patient);
        
        patient = patientService.fetch(nonStaffServiceRequest).getServiceData(Patient.class);
        if (patient != null) {
            log.info("*** TEST - testFetchNonStaff - RETURN OK - [" + patient + "]");
        } else {
            log.info("*** TEST - testFetchNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testDeleteStaff() {
        staffServiceRequest.setAction("DELETE");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to delete, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Patient patient = new Patient();
        patient.setId(1);
        staffServiceRequest.setServiceData(patient);
        
        Boolean isDeleted = patientService.delete(staffServiceRequest).getServiceData(Boolean.class);
        if (isDeleted) {
            log.info("*** TEST - testDeleteStaff - DELETE OK - [" + patient + "]");
        } else {
            log.info("*** TEST - testDeleteStaff - DELETE FAIL");
        }
        // PASS
    }
    
    //@Test
    public void testDeleteNonStaff() {
        nonStaffServiceRequest.setAction("DELETE");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to delete, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Patient patient = new Patient();
        patient.setId(1);
        nonStaffServiceRequest.setServiceData(patient);
        
        Boolean isDeleted = patientService.delete(nonStaffServiceRequest).getServiceData(Boolean.class);
        if (isDeleted) {
            log.info("*** TEST - testDeleteNonStaff - DELETE OK - ["  + patient + "]");
        } else {
            log.info("*** TEST - testDeleteNonStaff - DELETE FAIL");
        }
        // PASS
    }
    
    //@Test
    public void testListStaff() {
        staffServiceRequest.setAction("LIST");
        ServicePageRequest servicePageRequest = new ServicePageRequest();
        staffServiceRequest.setServiceData(servicePageRequest);

        JavaType type = new ObjectMapper().getTypeFactory().constructParametricType(ServicePageResponse.class, Patient.class);
        ServicePageResponse<Patient> queryResponse = patientService.list(staffServiceRequest).getServiceData(type);
        if (queryResponse != null) {
            List<Patient> list = queryResponse.getObjects();
            log.info("*** TEST - testListNonStaff - RETURN OK - " + list);
        } else {
            log.info("*** TEST - testListStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testListNonStaff() {
        nonStaffServiceRequest.setAction("LIST");
        ServicePageRequest servicePageRequest = new ServicePageRequest();
        nonStaffServiceRequest.setServiceData(servicePageRequest);

        JavaType type = new ObjectMapper().getTypeFactory().constructParametricType(ServicePageResponse.class, Patient.class);
        ServicePageResponse<Patient> queryResponse = patientService.list(nonStaffServiceRequest).getServiceData(type);
        if (queryResponse != null) {
            List<Patient> list = queryResponse.getObjects();
            log.info("*** TEST - testListNonStaff - RETURN OK - " + list);
        } else {
            log.info("*** TEST - testListNonStaff - RETURN NULL");
        }
        // PASS
    }
    
}