package au.com.paycorp.service.echannelling.service;

import au.com.paycorp.service.echannelling.entity.Specialty;
import au.com.paycorp.service.interfac.domain.ServiceRequest;
import au.com.paycorp.service.interfac.domain.ServicePageRequest;
import au.com.paycorp.service.interfac.domain.ServicePageResponse;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.apache.log4j.Logger;
import com.google.common.collect.Lists;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApplicationContext.xml"})
@Transactional
public class SpecialtyServiceUT {
    
    @Autowired
    private SpecialtyService specialtyService;
    private static final Logger log = Logger.getLogger(SpecialtyServiceUT.class);
    private static final ServiceRequest staffServiceRequest = new ServiceRequest();
    private static final ServiceRequest nonStaffServiceRequest = new ServiceRequest();
    static {
        staffServiceRequest.setRequestId("12345");
        staffServiceRequest.setResource("SPECIALTY_SERVICE");
        staffServiceRequest.setStaff(true);
        staffServiceRequest.setCustomerId(50000001);
        
        nonStaffServiceRequest.setRequestId("12345");
        nonStaffServiceRequest.setResource("SPECIALTY_SERVICE");
        nonStaffServiceRequest.setStaff(false);
        nonStaffServiceRequest.setCustomerId(50000273);
        nonStaffServiceRequest.setClients(Lists.newArrayList(10000005, 10003143, 10000167, 10004172, 10000970));
    }
    
    //@Test
    public void testAddStaff() {
        staffServiceRequest.setAction("ADD");
        Specialty specialty = new Specialty("test.name", "test.description");
        staffServiceRequest.setServiceData(specialty);
        
        specialty = specialtyService.add(staffServiceRequest).getServiceData(Specialty.class);
        if (specialty != null) {
            log.info("*** TEST - testAddStaff - RETURN OK - [" + specialty + "]");
        } else {
            log.info("*** TEST - testAddStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testAddNonStaff() {
        nonStaffServiceRequest.setAction("ADD");
        Specialty specialty = new Specialty("test.name", "test.description");
        nonStaffServiceRequest.setServiceData(specialty);
        
        specialty = specialtyService.add(nonStaffServiceRequest).getServiceData(Specialty.class);
        if (specialty != null) {
            log.info("*** TEST - testAddNonStaff - RETURN OK - [" + specialty + "]");
        } else {
            log.info("*** TEST - testAddNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testChangeStaff() {
        staffServiceRequest.setAction("CHANGE");
        Specialty specialty = new Specialty("test.name", "test.description");
        specialty.setId(1);
        staffServiceRequest.setServiceData(specialty);
        
        specialty = specialtyService.change(staffServiceRequest).getServiceData(Specialty.class);
        if (specialty != null) {
            log.info("*** TEST - testChangeStaff - RETURN OK - [" +  specialty + "]");
        } else {
            log.info("*** TEST - testChangeStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testChangeNonStaff() {
        nonStaffServiceRequest.setAction("CHANGE");
        Specialty specialty = new Specialty("test.name", "test.description");
        specialty.setId(1);
        nonStaffServiceRequest.setServiceData(specialty);
        
        specialty = specialtyService.change(nonStaffServiceRequest).getServiceData(Specialty.class);
        if (specialty != null) {
            log.info("*** TEST - testChangeNonStaff - RETURN OK - [" + specialty + "]");
        } else {
            log.info("*** TEST - testChangeNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testFetchStaff() {
        staffServiceRequest.setAction("FETCH");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to fech, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Specialty specialty = new Specialty();
        specialty.setId(1);
        staffServiceRequest.setServiceData(specialty);
        
        specialty = specialtyService.fetch(staffServiceRequest).getServiceData(Specialty.class);
        if (specialty != null) {
            log.info("*** TEST - testFetchStaff - RETURN OK - [" + specialty + "]");
        } else {
            log.info("*** TEST - testFetchStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testFetchNonStaff() {
        nonStaffServiceRequest.setAction("FETCH");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to fech, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Specialty specialty = new Specialty();
        specialty.setId(1);
        nonStaffServiceRequest.setServiceData(specialty);
        
        specialty = specialtyService.fetch(nonStaffServiceRequest).getServiceData(Specialty.class);
        if (specialty != null) {
            log.info("*** TEST - testFetchNonStaff - RETURN OK - [" + specialty + "]");
        } else {
            log.info("*** TEST - testFetchNonStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testDeleteStaff() {
        staffServiceRequest.setAction("DELETE");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to delete, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Specialty specialty = new Specialty();
        specialty.setId(1);
        staffServiceRequest.setServiceData(specialty);
        
        Boolean isDeleted = specialtyService.delete(staffServiceRequest).getServiceData(Boolean.class);
        if (isDeleted) {
            log.info("*** TEST - testDeleteStaff - DELETE OK - [" + specialty + "]");
        } else {
            log.info("*** TEST - testDeleteStaff - DELETE FAIL");
        }
        // PASS
    }
    
    //@Test
    public void testDeleteNonStaff() {
        nonStaffServiceRequest.setAction("DELETE");
        /*----------------------------------------------------------------------
        Remarks: The entity must exist in the database inorder to delete, otherwise
        you are risking an 'EmptyResultDataAccessException'.
        ----------------------------------------------------------------------*/
        Specialty specialty = new Specialty();
        specialty.setId(1);
        nonStaffServiceRequest.setServiceData(specialty);
        
        Boolean isDeleted = specialtyService.delete(nonStaffServiceRequest).getServiceData(Boolean.class);
        if (isDeleted) {
            log.info("*** TEST - testDeleteNonStaff - DELETE OK - ["  + specialty + "]");
        } else {
            log.info("*** TEST - testDeleteNonStaff - DELETE FAIL");
        }
        // PASS
    }
    
    //@Test
    public void testListStaff() {
        staffServiceRequest.setAction("LIST");
        ServicePageRequest servicePageRequest = new ServicePageRequest();
        staffServiceRequest.setServiceData(servicePageRequest);

        JavaType type = new ObjectMapper().getTypeFactory().constructParametricType(ServicePageResponse.class, Specialty.class);
        ServicePageResponse<Specialty> queryResponse = specialtyService.list(staffServiceRequest).getServiceData(type);
        if (queryResponse != null) {
            List<Specialty> list = queryResponse.getObjects();
            log.info("*** TEST - testListNonStaff - RETURN OK - " + list);
        } else {
            log.info("*** TEST - testListStaff - RETURN NULL");
        }
        // PASS
    }
    
    //@Test
    public void testListNonStaff() {
        nonStaffServiceRequest.setAction("LIST");
        ServicePageRequest servicePageRequest = new ServicePageRequest();
        nonStaffServiceRequest.setServiceData(servicePageRequest);

        JavaType type = new ObjectMapper().getTypeFactory().constructParametricType(ServicePageResponse.class, Specialty.class);
        ServicePageResponse<Specialty> queryResponse = specialtyService.list(nonStaffServiceRequest).getServiceData(type);
        if (queryResponse != null) {
            List<Specialty> list = queryResponse.getObjects();
            log.info("*** TEST - testListNonStaff - RETURN OK - " + list);
        } else {
            log.info("*** TEST - testListNonStaff - RETURN NULL");
        }
        // PASS
    }
    
}